﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsTraining
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("START SIMULATION");

            Subscriber mati = new Subscriber("Mati");
            Subscriber samanta = new Subscriber("Samanta");

            Publisher adi = new Publisher("Adi");
            Publisher kamila = new Publisher("Kamila");

            adi.Create("Kraina Lodu I", "Bajka", PublishType.Video);

            mati.Subscribe(adi);
            mati.Subscribe(kamila);
            samanta.Subscribe(adi);

            adi.Create("Toy Story", "Bajka", PublishType.Video);
            adi.Create("Akademia Pana Kleksa", "Bajka", PublishType.Book);
            Console.WriteLine("-----------------------------");

            kamila.Create("Joker", "Dramat", PublishType.Video);

            mati.UnSubscribe(adi);

            adi.Create("Skyfall", "Przygodowy", PublishType.Video);
            adi.Create("Html i CSS", "Informatyka", PublishType.Book);

            Console.WriteLine("-----------------------------");
            kamila.Create("4 Pory Roku", "Muzyka Poważna", PublishType.Music);
            kamila.Create("7 nawyków zdrowego odżywiania", "Dieta", PublishType.Book);
            
            Console.WriteLine("END SIMULATION");
            Console.ReadLine();
        }
    }
}

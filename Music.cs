﻿using System;

namespace EventsTraining
{
    internal class Music : IPublishable
    {
        public string Author { get; set; }
        public DateTime CreateDate { get; set; }
        public string Genre { get; set; }
        public string Title { get; set; }
    }
}
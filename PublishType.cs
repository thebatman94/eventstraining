﻿
namespace EventsTraining
{
    public enum PublishType
    {
        Video,
        Book,
        Music,
        Game
    }
}

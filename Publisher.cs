﻿using System;

namespace EventsTraining
{
    public class Publisher
    {
        public delegate void NotifySubscribersDelegate(Publisher publisher, IPublishable medium);
        public event NotifySubscribersDelegate NewMedium;

        public string Name { get; }

        public Publisher(string name)
        {
            Name = name;
        }

        public void Create(string title, string genre, PublishType type)
        {
            switch (type)
            {
                case PublishType.Video:
                    Publish(new Video()
                    {
                        Author = Name,
                        CreateDate = DateTime.Now,
                        Genre = genre,
                        Title = title
                    });
                    break;

                case PublishType.Book:
                    Publish(new Book()
                    {
                        Author = Name,
                        CreateDate = DateTime.Now,
                        Genre = genre,
                        Title = title
                    });
                    break;

                case PublishType.Music:
                    Publish(new Music()
                    {
                        Author = Name,
                        CreateDate = DateTime.Now,
                        Genre = genre,
                        Title = title
                    });
                    break;
            }
        }

        private void Publish(IPublishable medium)
        {
            NewMedium?.Invoke(this, medium);
        }

    }
}
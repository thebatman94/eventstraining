﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsTraining
{
    public class Book : IPublishable
    {
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        public string Genre { get; set; }
        public string Author { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsTraining
{
    public interface IPublishable
    {
        string Title { get; set; }
        DateTime CreateDate {get; set;}
        string Genre { get; set; }
        string Author { get; set; }
    }
}

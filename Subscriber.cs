﻿using System;

namespace EventsTraining
{
    public class Subscriber
    {
        public string Name { get;  }
        
        //Not needed.
        //public List<Publisher> Publishers { get; set; }

        public Subscriber(string name)
        {
            Name = name;
        }

        public void Subscribe(Publisher publisher)
        {
            publisher.NewMedium += GetNotify;
        }

        public void UnSubscribe(Publisher publisher)
        {
            publisher.NewMedium -= GetNotify;
        }

        private void GetNotify(Publisher p, IPublishable m)
        {
            Console.WriteLine($"*{Name}*, You got new notified from *{p.Name}* published: '{m.Title}'.");
        }
    }
}
